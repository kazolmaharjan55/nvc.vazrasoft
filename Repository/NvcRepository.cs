﻿using Dapper;
using Entities;
using RepositoryInterface;
using System;
using static System.Data.CommandType;


namespace Repository
{
    public class NvcRepository : BaseRepository, IRepository<Nvc>
    {
        public void Add(Nvc entity)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RegistrationNo", entity.RegistrationNo);
                parameters.Add("@CourseNumber",entity.CourseNumber );
                parameters.Add("@ApplicationNo", entity.ApplicationNo);
                parameters.Add("@isOldStudent",entity.isOldStudent );
                parameters.Add("@isCategoryToCenter", entity.isCategoryToCenter);
                parameters.Add("@isForeigner", entity.isForeigner);
                parameters.Add("@isMonk", entity.isMonk);
                parameters.Add("@FirstName", entity.FirstName);
                parameters.Add("@MiddleName", entity.MiddleName);
                parameters.Add("@LastName", entity.LastName);
                parameters.Add("@Address", entity.Address);
                parameters.Add("@District", entity.District);
                parameters.Add("@Country", entity.Country);
                parameters.Add("@Profession", entity.Profession);
                parameters.Add("@Education", entity.Education);
                parameters.Add("@HomePhone", entity.HomePhone);
                parameters.Add("@OfficePhone", entity.OfficePhone);
                parameters.Add("@MobileNo", entity.MobileNo);
                parameters.Add("@Age", entity.Age);
                parameters.Add("@Gender", entity.Age);
                parameters.Add("@DOB", entity.DOB);
                parameters.Add("@NepaliDOB", entity.NeapliDOB);
                parameters.Add("@Email", entity.Email);
                parameters.Add("@isMarried", entity.isMarried);
                parameters.Add("@GuardianName", entity.GuardianName);
                parameters.Add("@isApplicationCancelled", entity.isApplicationCancelled);
                parameters.Add("@AnyRelativeOrFriendAttending", entity.AnyRelativeOrFriendAttending);
                parameters.Add("@isHindi", entity.isHindi);
                parameters.Add("@isEnglish", entity.isEnglish);
                parameters.Add("@isNepali", entity.isNepali);
                parameters.Add("@isNewari", entity.isNewari);
                parameters.Add("@AnyOtherLanguage", entity.AnyOtherLanguage);
                parameters.Add("@hasPhysicalOrMentalDisease", entity.hasPhysicalOrMentalDisease);
                parameters.Add("@PhysicalOrMentalDisease", entity.PhysicalOrMentalDisease);
                parameters.Add("@Medication", entity.Medication);
                parameters.Add("@PreiousPractice", entity.PreviousPractice);
                parameters.Add("@Drugs", entity.Drugs);
                parameters.Add("@QuitInToxitant", entity.QuitInToxitant);
                parameters.Add("@DrugUseDetails", entity.DrugUseDetails);
                parameters.Add("@FirstCourseDate", entity.FirstCourseDate);
                parameters.Add("@FirstCourseLocation", entity.FirstCourseLocation);
                parameters.Add("@FirstCourseTeacherName", entity.FirstCourseTeacherName);
                parameters.Add("@LastCourseDate", entity.LastCourseDate);
                parameters.Add("@LastCourseLocation", entity.LastCourseLocation);
                parameters.Add("@LastCourseTeacherName", entity.LastCourseTeacherName);
                parameters.Add("@AnyPrevMedicationTech", entity.AnyPrevMedicationTech);
                parameters.Add("@PrevTherapyDetails", entity.PrevTherapyDetails);
                parameters.Add("@10DayWithGoenkaji", entity._10DayWithGoenkaji);
                parameters.Add("@10DayOther", entity._10DayOther);
                parameters.Add("@Satipathan", entity.Satipathan);
                parameters.Add("@Special10Day", entity.Special10Day);
                parameters.Add("@20Day", entity._20Day);
                parameters.Add("@30Day", entity._30Day);
                parameters.Add("@45Or60Day", entity._4560Day);
                parameters.Add("@TSC", entity.TSC);
                parameters.Add("@NumberOfCourseServed", entity.NumberOfCoursesServed);
                parameters.Add("@ContinuityOfDailyPractice", entity.ContinuityOfDailyPractice);
                parameters.Add("@DailyMedicationDuration", entity.DailyMedicationDuration);
                parameters.Add("@EarlyVolunteerInRday", entity.EarlyVolunteerInRday);
                parameters.Add("@ServeCourse", entity.ServeCourse);
                parameters.Add("@PartialServiceStartDate", entity.PartialServiceStartDate);
                parameters.Add("@PartialServiceEndDate", entity.PartialServiceEndDate);
                parameters.Add("@PartialStudentStartDate", entity.PartialStudentStartDate);
                parameters.Add("@PartialStudentEndDate", entity.PartialStudentEndDate);
                parameters.Add("@PersonalSummary1", entity.PersonalSummary1);
                parameters.Add("@PrevPracticeInFormsOfMedication", entity.PrevPracticeInFormsOfMedication);
                parameters.Add("@PrevPracticeDetailInFormOFMedication", entity.PrevPracticeDetailInFormOFMedication);
                parameters.Add("@KnowAboutVipasanaMedication", entity.KnowAboutVipasanaMedication);
                parameters.Add("@RefererName", entity.RefererName);
                parameters.Add("@RefererAddress", entity.RefererAddress);
                parameters.Add("@RefererPhone", entity.RefererPhone);
                parameters.Add("@RefererLatestCourse", entity.RefererLatestCourse);
                parameters.Add("@RefererLatestCourseDate", entity.RefererLatestCourseDate);
                parameters.Add("@RefererLatestCourseTeacher", entity.RefererLatestCourseTeacher);
                parameters.Add("@ReferenceDate", entity.ReferenceDate);
                parameters.Add("@ReadCodeOfConduct", entity.ReadCodeOfConduct);
                parameters.Add("@UnderstoodCodeOfConduct", entity.UnderstoodCodeOfConduct);
                parameters.Add("@AbideByRulesOfCourse", entity.AbideByRulesOfCourse);
                parameters.Add("@StayInsideCenter", entity.StayInsideCenter);
                parameters.Add("@MaintainSilence", entity.MaintainSilence);
                parameters.Add("@PersonalSummary2", entity.PersonalSummary2);
                parameters.Add("@Name", entity.Name);
                parameters.Add("@Relation", entity.Relation);
                parameters.Add("@AddressE", entity.AddressE);
                parameters.Add("@Phone", entity.Phone);

                SqlMapper.Execute(con, "SpNvcIns", parameters, commandType:StoredProcedure);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(Nvc entity, int id)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@ApplicationId", id);
                parameters.Add("@RegistrationNo", entity.RegistrationNo);
                parameters.Add("@CourseNumber", entity.CourseNumber);
                parameters.Add("@ApplicationNo", entity.ApplicationNo);
                parameters.Add("@isOldStudent", entity.isOldStudent);
                parameters.Add("@isCategoryToCenter", entity.isCategoryToCenter);
                parameters.Add("@isForeigner", entity.isForeigner);
                parameters.Add("@isMonk", entity.isMonk);
                parameters.Add("@FirstName", entity.FirstName);
                parameters.Add("@MiddleName", entity.MiddleName);
                parameters.Add("@LastName", entity.LastName);
                parameters.Add("@Address", entity.Address);
                parameters.Add("@District", entity.District);
                parameters.Add("@Country", entity.Country);
                parameters.Add("@Profession", entity.Profession);
                parameters.Add("@Education", entity.Education);
                parameters.Add("@HomePhone", entity.HomePhone);
                parameters.Add("@OfficePhone", entity.OfficePhone);
                parameters.Add("@MobileNo", entity.MobileNo);
                parameters.Add("@Age", entity.Age);
                parameters.Add("@Gender", entity.Age);
                parameters.Add("@DOB", entity.DOB);
                parameters.Add("@NepaliDOB", entity.NeapliDOB);
                parameters.Add("@Email", entity.Email);
                parameters.Add("@isMarried", entity.isMarried);
                parameters.Add("@GuardianName", entity.GuardianName);
                parameters.Add("@isApplicationCancelled", entity.isApplicationCancelled);
                parameters.Add("@AnyRelativeOrFriendAttending", entity.AnyRelativeOrFriendAttending);
                parameters.Add("@isHindi", entity.isHindi);
                parameters.Add("@isEnglish", entity.isEnglish);
                parameters.Add("@isNepali", entity.isNepali);
                parameters.Add("@isNewari", entity.isNewari);
                parameters.Add("@AnyOtherLanguage", entity.AnyOtherLanguage);
                parameters.Add("@hasPhysicalOrMentalDisease", entity.hasPhysicalOrMentalDisease);
                parameters.Add("@PhysicalOrMentalDisease", entity.PhysicalOrMentalDisease);
                parameters.Add("@Medication", entity.Medication);
                parameters.Add("@PreiousPractice", entity.PreviousPractice);
                parameters.Add("@Drugs", entity.Drugs);
                parameters.Add("@QuitInToxitant", entity.QuitInToxitant);
                parameters.Add("@DrugUseDetails", entity.DrugUseDetails);
                parameters.Add("@FirstCourseDate", entity.FirstCourseDate);
                parameters.Add("@FirstCourseLocation", entity.FirstCourseLocation);
                parameters.Add("@FirstCourseTeacherName", entity.FirstCourseTeacherName);
                parameters.Add("@LastCourseDate", entity.LastCourseDate);
                parameters.Add("@LastCourseLocation", entity.LastCourseLocation);
                parameters.Add("@LastCourseTeacherName", entity.LastCourseTeacherName);
                parameters.Add("@AnyPrevMedicationTech", entity.AnyPrevMedicationTech);
                parameters.Add("@PrevTherapyDetails", entity.PrevTherapyDetails);
                parameters.Add("@10DayWithGoenkaji", entity._10DayWithGoenkaji);
                parameters.Add("@10DayOther", entity._10DayOther);
                parameters.Add("@Satipathan", entity.Satipathan);
                parameters.Add("@Special10Day", entity.Special10Day);
                parameters.Add("@20Day", entity._20Day);
                parameters.Add("@30Day", entity._30Day);
                parameters.Add("@45Or60Day", entity._4560Day);
                parameters.Add("@TSC", entity.TSC);
                parameters.Add("@NumberOfCourseServed", entity.NumberOfCoursesServed);
                parameters.Add("@ContinuityOfDailyPractice", entity.ContinuityOfDailyPractice);
                parameters.Add("@DailyMedicationDuration", entity.DailyMedicationDuration);
                parameters.Add("@EarlyVolunteerInRday", entity.EarlyVolunteerInRday);
                parameters.Add("@ServeCourse", entity.ServeCourse);
                parameters.Add("@PartialServiceStartDate", entity.PartialServiceStartDate);
                parameters.Add("@PartialServiceEndDate", entity.PartialServiceEndDate);
                parameters.Add("@PartialStudentStartDate", entity.PartialStudentStartDate);
                parameters.Add("@PartialStudentEndDate", entity.PartialStudentEndDate);
                parameters.Add("@PersonalSummary1", entity.PersonalSummary1);
                parameters.Add("@PrevPracticeInFormsOfMedication", entity.PrevPracticeInFormsOfMedication);
                parameters.Add("@PrevPracticeDetailInFormOFMedication", entity.PrevPracticeDetailInFormOFMedication);
                parameters.Add("@KnowAboutVipasanaMedication", entity.KnowAboutVipasanaMedication);
                parameters.Add("@RefererName", entity.RefererName);
                parameters.Add("@RefererAddress", entity.RefererAddress);
                parameters.Add("@RefererPhone", entity.RefererPhone);
                parameters.Add("@RefererLatestCourse", entity.RefererLatestCourse);
                parameters.Add("@RefererLatestCourseDate", entity.RefererLatestCourseDate);
                parameters.Add("@RefererLatestCourseTeacher", entity.RefererLatestCourseTeacher);
                parameters.Add("@ReferenceDate", entity.ReferenceDate);
                parameters.Add("@ReadCodeOfConduct", entity.ReadCodeOfConduct);
                parameters.Add("@UnderstoodCodeOfConduct", entity.UnderstoodCodeOfConduct);
                parameters.Add("@AbideByRulesOfCourse", entity.AbideByRulesOfCourse);
                parameters.Add("@StayInsideCenter", entity.StayInsideCenter);
                parameters.Add("@MaintainSilence", entity.MaintainSilence);
                parameters.Add("@PersonalSummary2", entity.PersonalSummary2);
                parameters.Add("@Name", entity.Name);
                parameters.Add("@Relation", entity.Relation);
                parameters.Add("@AddressE", entity.AddressE);
                parameters.Add("@Phone", entity.Phone);

                SqlMapper.Execute(con, "SpNvcUpd", parameters, commandType: StoredProcedure);

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
