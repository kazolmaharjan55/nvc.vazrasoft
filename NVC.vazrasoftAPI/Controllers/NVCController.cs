﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RepositoryInterface;
using Repository;
using Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NVC.vazrasoftAPI.Controllers
{
    [Route("api/[controller]")]
    public class NVCController : Controller
    {
        IRepository<Nvc> _repository;

        public NVCController(IRepository<Nvc> repository)
        {
            repository = _repository;
        }

        // GET: api/<controller>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/<controller>/5
       /* [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }*/

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Nvc value)
        {
            _repository.Add(value);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Nvc value)
        {
            _repository.Update(value, id);
        }

        // DELETE api/<controller>/5
        /*[HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
