﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RepositoryInterface
{
    public interface IRepository <T> where T : class
    {
        void Add(T entity);
        void Update(T entity, int id);
    }
}
