﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Entities
{
    public class Nvc
    {
        public int RegistrationNo { get; set; }
        public int CourseNumber { get; set; }
        public int ApplicationNo { get; set; }

        public bool isOldStudent { get; set; }
        public List<SelectListItem> yesno { get; set; }

        public bool isCategoryToCenter { get; set; }
        public bool isForeigner { get; set; }
        public bool isMonk { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string Country { get; set; }
        public string Profession { get; set; }
        public string Education { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string MobileNo { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public DateTime NeapliDOB { get; set; }
        public string Email { get; set; }
        public bool isMarried { get; set; }
        public string GuardianName { get; set; }
        public bool isApplicationCancelled { get; set; }
        public bool AnyRelativeOrFriendAttending { get; set; }
        public bool isHindi { get; set; }
        public bool isEnglish { get; set; }
        public bool isNepali { get; set; }
        public bool isNewari { get; set; }
        public bool AnyOtherLanguage { get; set; }
        public bool hasPhysicalOrMentalDisease { get; set; }
        public string PhysicalOrMentalDisease { get; set; }
        public string Medication { get; set; }
        public bool PreviousPractice { get; set; }
        public bool Drugs { get; set; }
        public bool QuitInToxitant { get; set; }
        public string DrugUseDetails { get; set; }
        public DateTime FirstCourseDate { get; set; }
        public string FirstCourseLocation { get; set; }
        public string FirstCourseTeacherName { get; set; }
        public DateTime LastCourseDate { get; set; }
        public string LastCourseLocation { get; set; }
        public string LastCourseTeacherName { get; set; }
        public bool AnyPrevMedicationTech { get; set; }
        public string PrevTherapyDetails { get; set; }
        public string _10DayWithGoenkaji { get; set; }
        public string _10DayOther { get; set; }
        public string Satipathan { get; set; }
        public string Special10Day { get; set; }
        public string _20Day { get; set; }
        public string _30Day { get; set; }
        public string _4560Day { get; set; }
        public string TSC { get; set; }
        public string NumberOfCoursesServed { get; set; }
        public bool ContinuityOfDailyPractice { get; set; }
        public string DailyMedicationDuration { get; set; }
        public bool EarlyVolunteerInRday { get; set; }
        public bool ServeCourse { get; set; }
        public DateTime PartialServiceStartDate { get; set; }
        public DateTime PartialServiceEndDate { get; set; }
        public DateTime PartialStudentStartDate { get; set; }
        public DateTime PartialStudentEndDate { get; set; }
        public string PersonalSummary1 { get; set; }
        public bool PrevPracticeInFormsOfMedication { get; set; }
        public string PrevPracticeDetailInFormOFMedication { get; set; }
        public string KnowAboutVipasanaMedication { get; set; }
        public string RefererName { get; set; }
        public string RefererAddress { get; set; }
        public string RefererPhone { get; set; }
        public string RefererLatestCourse { get; set; }
        public DateTime RefererLatestCourseDate { get; set; }
        public string RefererLatestCourseTeacher { get; set; }
        public DateTime ReferenceDate { get; set; }
        public bool ReadCodeOfConduct { get; set; }
        public bool UnderstoodCodeOfConduct { get; set; }
        public bool AbideByRulesOfCourse { get; set; }
        public bool StayInsideCenter { get; set; }
        public bool MaintainSilence { get; set; }
        public string PersonalSummary2 { get; set; }
        public string Name { get; set; }
        public string Relation { get; set; }
        public string AddressE { get; set; }
        public string Phone { get; set; }

    }
}
