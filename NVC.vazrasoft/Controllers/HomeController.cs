﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entities;

namespace NVC.vazrasoft.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Nvc nvc = new Nvc();
            nvc.yesno = new List<SelectListItem>
            {
                new SelectListItem {Text="Yes",Value="1"},
                new SelectListItem {Text="No",Value="0"}
            };
            return View(nvc);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}